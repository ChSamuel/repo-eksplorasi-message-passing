from datetime import datetime
import pika, sys, os

def main():
    def callback(ch, method, properties, body):
        body_in_string = body.decode('utf-8')
        print('[%s] %s' % (method.routing_key.upper(), body_in_string))
    
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()

    channel.exchange_declare(exchange='simple_log_exchange', exchange_type='direct')

    result = channel.queue_declare(queue='', exclusive=True)
    random_queue_name = result.method.queue

    signs = sys.argv[1:]
    if not signs:
        sys.stderr.write("Nyalakan sebuah logger dengan: %s [positif]|[nol]|[negatif]\n" % sys.argv[0])
        sys.exit(1)
    
    for sign in signs:
        channel.queue_bind(
            queue=random_queue_name,
            exchange='simple_log_exchange',
            routing_key=sign
            )

    channel.basic_consume(
        queue=random_queue_name,
        on_message_callback=callback,
        auto_ack=True
    )

    print('Logger siap menulis di terminal. Berhenti dengan CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Logger dimatikan')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
