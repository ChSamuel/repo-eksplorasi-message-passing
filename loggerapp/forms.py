from django import forms

class AddForm(forms.Form):
    parameter_1 = forms.IntegerField()
    parameter_2 = forms.IntegerField()
