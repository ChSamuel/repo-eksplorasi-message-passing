from django.shortcuts import render
from datetime import datetime, timezone
from .forms import *
import pika, sys

def index(request):
    if request.method == 'POST':
        a = int(request.POST.get('parameter_1'))
        b = int(request.POST.get('parameter_2'))

        if a + b == 0:
            jenis_bilangan = 'nol'
        elif a + b > 0:
            jenis_bilangan = 'positif'
        else:
            jenis_bilangan = 'negatif'
        
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()

        channel.exchange_declare(exchange='simple_log_exchange', exchange_type='direct')

        message = 'Ada request pada ' + str(datetime.now()) + ' WIB '
        message += 'dengan hasil bilangan ' + str(a+b)
        channel.basic_publish(
            exchange='simple_log_exchange',
            routing_key=jenis_bilangan,
            body=message
        )

        connection.close()

    form = AddForm()
    return render(request, 'index.html', {
        'form' : form,
    })
